# Changelog for [`os-string` package](http://hackage.haskell.org/package/os-string)

## 1.0.0 *??? 2023*

* Split out `OsString` from filepath package

